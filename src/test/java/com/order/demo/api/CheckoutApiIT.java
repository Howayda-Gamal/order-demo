package com.order.demo.api;

import com.order.demo.OrderDemoApplication;
import com.order.demo.model.CheckoutResponse;
import com.order.demo.model.Order;
import com.order.demo.utils.GenerateStubs;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderDemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CheckoutApiIT {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void checkoutSuccessIT(){

        Order order = GenerateStubs.generateOrderAvailable(101);
        HttpEntity<Order> entity = new HttpEntity<Order>(order,headers);

        ResponseEntity<CheckoutResponse> response = restTemplate.exchange(
                establishPortAndURIPort("/api/checkout"),
                HttpMethod.POST, entity, CheckoutResponse.class);

        CheckoutResponse expected = CheckoutResponse.builder().message("Success").statusCode("0").build();

        Assertions.assertEquals(expected, response.getBody());
    }

    @Test
    public void checkoutFailedIT(){

        Order order = GenerateStubs.generateOrderNotAvailable(101);
        HttpEntity<Order> entity = new HttpEntity<Order>(order,headers);

        ResponseEntity<CheckoutResponse> response = restTemplate.exchange(
                establishPortAndURIPort("/api/checkout"),
                HttpMethod.POST, entity, CheckoutResponse.class);

        CheckoutResponse expected = CheckoutResponse.builder().message("Failed to checkout").statusCode("1").build();

        Assertions.assertEquals(expected, response.getBody());
    }

    private String establishPortAndURIPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
