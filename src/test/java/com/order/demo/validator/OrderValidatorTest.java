package com.order.demo.validator;

import com.order.demo.configuration.OrderPropertyConfiguration;
import com.order.demo.utils.GenerateStubs;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class OrderValidatorTest {

    @Mock
    OrderPropertyConfiguration orderPropertyConfiguration;

    @InjectMocks
    OrderValidator orderValidator;

    @Mock
    OrderPropertyConfiguration.Validation validation;

    @Before
    public void setUp() throws Exception {
        Mockito.when((orderPropertyConfiguration.getValidation())).thenReturn(validation);
        Mockito.when(validation.getMinOrderAmount()).thenReturn("100");
        Mockito.when(validation.getMinOrderBasketAmount()).thenReturn("1500");
    }

    @Test
    public void checkItemsAvailabilitySuccessTest(){

        Boolean availabilityResult = orderValidator.checkItemsAvailability(GenerateStubs.generateItemListAllAvailable());
        Assertions.assertEquals(Boolean.TRUE, availabilityResult);
    }

    @Test
    public void checkItemsAvailabilityFailureTest(){

        Boolean availabilityResult = orderValidator.checkItemsAvailability(GenerateStubs.generateItemListAllUnavailable());
        Assertions.assertEquals(Boolean.FALSE, availabilityResult);
    }

    @Test
    public void validateOrderAmountSuccessTest(){

        Boolean checkMinAmountResult = orderValidator.validateOrderAmount(GenerateStubs.generateOrderAvailable(700));
        Assertions.assertEquals(Boolean.TRUE, checkMinAmountResult);
    }

    @Test
    public void validateOrderAmountFailureTest(){

        Boolean checkMinAmountResult = orderValidator.validateOrderAmount(GenerateStubs.generateOrderAvailable(50));
        Assertions.assertEquals(Boolean.FALSE, checkMinAmountResult);
    }

    @Test
    public void isFraudDetectedSuccessTest(){

        Boolean checkMinAmountResult = orderValidator.isFraudDetected(GenerateStubs.generateOrderAvailable(1300));
        Assertions.assertEquals(Boolean.FALSE, checkMinAmountResult);
    }

    @Test
    public void isFraudDetectedFailureTest(){

        Boolean checkMinAmountResult = orderValidator.isFraudDetected(GenerateStubs.generateOrderAvailable(1900));
        Assertions.assertEquals(Boolean.TRUE, checkMinAmountResult);
    }
}
