package com.order.demo.utils;

import com.order.demo.model.Card;
import com.order.demo.model.Item;
import com.order.demo.model.Order;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@Builder
public class GenerateStubs {

    public static Order generateOrderAvailable(double totalAmount){

        List<Item> itemList = new ArrayList<>();
        itemList.add(generateItem(Boolean.TRUE));
        itemList.add(generateItem(Boolean.TRUE));

        Order order = Order.builder()
                .orderId(1)
                .totalAmount(totalAmount)
                .userId(1)
                .items(itemList)
                .build();

        return order;
    }

    public static Order generateOrderNotAvailable(double totalAmount){

        List<Item> itemList = new ArrayList<>();
        itemList.add(generateItem(Boolean.TRUE));
        itemList.add(generateItem(Boolean.FALSE));

        Order order = Order.builder()
                .orderId(1)
                .totalAmount(totalAmount)
                .userId(1)
                .items(itemList)
                .build();

        return order;
    }

    public static Item generateItem(boolean available){
        Item item = Item.builder()
                .itemId(1)
                .itemName("a")
                .available(available)
                .price(12)
                .build();
        return item;
    }

    public static ResponseEntity generateHTTPStatusOk(){
        return new ResponseEntity(HttpStatus.OK);
    }

    public static Card generateCard(){
        Card card = new Card();
        card.setCardId(1001);
        card.setCardNumber("123456789");
        card.setCardType("VISA");
        card.setCcv("111");
        card.setExpiryDate("01-01-2028");
        return card;

    }

    public static List<Item> generateItemListAllAvailable(){

        List<Item> items = new ArrayList<>();
        items.add(generateItem(Boolean.TRUE));
        items.add(generateItem(Boolean.TRUE));
        return items;
    }

    public static List<Item> generateItemListAllUnavailable(){

        List<Item> items = new ArrayList<>();
        items.add(generateItem(Boolean.FALSE));
        items.add(generateItem(Boolean.FALSE));
        return items;
    }
}
