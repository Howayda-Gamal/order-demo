package com.order.demo.service.impl;

import com.order.demo.manager.PaymentManager;
import com.order.demo.model.CheckoutResponse;
import com.order.demo.utils.Constants;
import com.order.demo.utils.GenerateStubs;
import com.order.demo.validator.OrderValidator;
import com.order.demo.ws.resource.PaymentGatewayResource;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CheckoutServiceImplTest {

    @Mock
    PaymentGatewayResource paymentGatewayResource;

    @Mock
    OrderValidator orderValidator;

    @Mock
    PaymentManager paymentManager;

    @InjectMocks
    CheckoutServiceImpl checkoutService;

    @Test
    public void checkoutOrderSuccessTest(){

        Mockito.when(paymentGatewayResource.chargeUser(ArgumentMatchers.any()))
                .thenReturn(GenerateStubs.generateHTTPStatusOk());

        Mockito.when(paymentManager.getCardByUserId(ArgumentMatchers.anyInt())).thenReturn(GenerateStubs.generateCard());

        Mockito.when(orderValidator.checkItemsAvailability(ArgumentMatchers.any())).thenReturn(Boolean.TRUE);
        Mockito.when(orderValidator.validateOrderAmount(ArgumentMatchers.any())).thenReturn(Boolean.TRUE);
        Mockito.when(orderValidator.isFraudDetected(ArgumentMatchers.any())).thenReturn(Boolean.FALSE);
        CheckoutResponse checkoutResponse = checkoutService.checkoutOrder(GenerateStubs.generateOrderNotAvailable(1000));

        Assertions.assertNotNull(checkoutResponse);
        Assertions.assertEquals(Constants.SUCCESS_STATUS_CODE,checkoutResponse.getStatusCode());
        Assertions.assertEquals(Constants.CHECKOUT_SUCCESS_MESSAGE,checkoutResponse.getMessage());
    }

    @Test
    public void checkoutOrderFailureTest(){

        Mockito.when(paymentGatewayResource.chargeUser(ArgumentMatchers.any()))
                .thenReturn(GenerateStubs.generateHTTPStatusOk());

        Mockito.when(paymentManager.getCardByUserId(ArgumentMatchers.anyInt())).thenReturn(GenerateStubs.generateCard());

        Mockito.when(orderValidator.checkItemsAvailability(ArgumentMatchers.any())).thenReturn(Boolean.TRUE);
        Mockito.when(orderValidator.validateOrderAmount(ArgumentMatchers.any())).thenReturn(Boolean.TRUE);
        Mockito.when(orderValidator.isFraudDetected(ArgumentMatchers.any())).thenReturn(Boolean.TRUE);
        CheckoutResponse checkoutResponse = checkoutService.checkoutOrder(GenerateStubs.generateOrderNotAvailable(1000));

        Assertions.assertNotNull(checkoutResponse);
        Assertions.assertEquals(Constants.FAILURE_STATUS_CODE,checkoutResponse.getStatusCode());
        Assertions.assertEquals(Constants.CHECKOUT_FAILURE_MESSAGE,checkoutResponse.getMessage());
    }
}
