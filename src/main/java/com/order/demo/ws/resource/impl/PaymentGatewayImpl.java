package com.order.demo.ws.resource.impl;

import com.order.demo.model.request.PaymentDetailsRequest;
import com.order.demo.ws.resource.PaymentGatewayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * dummy service to implement the payment gateway to implement to credit card integration
 */

@Component
public class PaymentGatewayImpl implements PaymentGatewayResource {

    @Override
    public ResponseEntity chargeUser(PaymentDetailsRequest chargeUserRequest) {
        if(chargeUserRequest.getCcv().length() == 3){
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
    }
}
