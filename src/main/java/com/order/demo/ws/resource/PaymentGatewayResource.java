package com.order.demo.ws.resource;


import com.order.demo.model.request.PaymentDetailsRequest;
import org.springframework.http.ResponseEntity;

public interface PaymentGatewayResource {

    ResponseEntity chargeUser(PaymentDetailsRequest paymentDetailsRequest);
}
