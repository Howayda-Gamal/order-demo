package com.order.demo.service.impl;

import com.order.demo.manager.PaymentManager;
import com.order.demo.model.Card;
import com.order.demo.model.CheckoutResponse;
import com.order.demo.model.Order;
import com.order.demo.model.request.PaymentDetailsRequest;
import com.order.demo.utils.Constants;
import com.order.demo.validator.OrderValidator;
import com.order.demo.ws.resource.PaymentGatewayResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CheckoutServiceImpl {

    private final OrderValidator orderValidator;

    private final PaymentManager paymentManager;

    private final PaymentGatewayResource paymentGatewayResource;

    public CheckoutResponse checkoutOrder(final Order order) {

        log.info("Start checkoutOrder ..");
        if (isOrderValid(order)) {
            Card card = paymentManager.getCardByUserId(order.getUserId());
            ResponseEntity responseEntity = paymentGatewayResource.chargeUser(PaymentDetailsRequest.builder()
                    .cardNumber(card.getCardNumber())
                    .ccv(card.getCcv())
                    .expiryDate(card.getExpiryDate())
                    .amount(order.getTotalAmount()).build());
            if (responseEntity != null && HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return CheckoutResponse.builder().statusCode(Constants.SUCCESS_STATUS_CODE)
                        .message(Constants.CHECKOUT_SUCCESS_MESSAGE).build();
            }
        }
        return CheckoutResponse.builder().statusCode(Constants.FAILURE_STATUS_CODE).message(Constants.CHECKOUT_FAILURE_MESSAGE).build();
    }

    private boolean isOrderValid(Order order) {

        if (orderValidator.checkItemsAvailability(order.getItems())
                && orderValidator.validateOrderAmount(order)
                && !orderValidator.isFraudDetected(order)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
