package com.order.demo.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "order")
@Data
public class OrderPropertyConfiguration {

    private Validation validation;

    @Data
    public static class Validation {

        private String minOrderAmount;

        private String minOrderBasketAmount;
    }
}
