package com.order.demo.utils;

public class Constants {

    public static final String CHECKOUT_FAILURE_MESSAGE = "Failed to checkout";

    public static final String CHECKOUT_SUCCESS_MESSAGE = "Success";

    public static final String SUCCESS_STATUS_CODE = "0";

    public static final String FAILURE_STATUS_CODE = "1";
}
