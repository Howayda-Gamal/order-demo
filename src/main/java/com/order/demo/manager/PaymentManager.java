package com.order.demo.manager;

import com.order.demo.model.Card;
import com.order.demo.repo.CardRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PaymentManager {

    private final CardRepo cardRepo;

    public Card getCardByUserId(Integer userId){

        return cardRepo.findByUserId(1);
    }
}
