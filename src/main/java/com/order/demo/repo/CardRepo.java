package com.order.demo.repo;

import com.order.demo.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepo extends JpaRepository<Card,Integer> {

    Card findByUserId(Integer userId);

}
