package com.order.demo.api;

import com.order.demo.model.CheckoutResponse;
import com.order.demo.model.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

public interface CheckoutApi {

    @PostMapping(path = "/checkout")
    ResponseEntity<CheckoutResponse> checkout(@Valid @RequestBody final Order order);
}
