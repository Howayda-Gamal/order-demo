package com.order.demo.api.impl;

import com.order.demo.api.CheckoutApi;
import com.order.demo.model.CheckoutResponse;
import com.order.demo.model.Order;
import com.order.demo.service.impl.CheckoutServiceImpl;
import com.order.demo.utils.Constants;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping(value = "/api")
@Api(tags = "")
@RestController
@Slf4j
@RequiredArgsConstructor
public class CheckoutApiImpl implements CheckoutApi {

    private final CheckoutServiceImpl checkoutServiceImpl;

    @PostMapping(path = "/checkout")
    public ResponseEntity<CheckoutResponse> checkout(@Valid @RequestBody final Order order) {

        final CheckoutResponse checkoutResponse = checkoutServiceImpl.checkoutOrder(order);
        if (Constants.SUCCESS_STATUS_CODE.equals(checkoutResponse.getStatusCode())) {
            return ResponseEntity.status(HttpStatus.OK).body(checkoutResponse);
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(checkoutResponse);
    }
}
