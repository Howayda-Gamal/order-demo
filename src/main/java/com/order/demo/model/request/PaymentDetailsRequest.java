package com.order.demo.model.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentDetailsRequest {

    private String cardNumber;

    private String  ccv;

    private String expiryDate;

    private double amount;
}
