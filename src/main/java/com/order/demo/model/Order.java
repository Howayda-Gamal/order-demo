package com.order.demo.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Builder
public class Order {

    private int orderId;

    @NotBlank
    private List<Item> items;

    @NotBlank
    private int userId;

    @NotBlank
    private double totalAmount;
}
