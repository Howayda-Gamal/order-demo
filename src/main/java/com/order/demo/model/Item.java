package com.order.demo.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Item {

    private int itemId;

    private String itemName;

    private double price;

    private boolean available;
}
