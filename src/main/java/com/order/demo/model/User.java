package com.order.demo.model;

import lombok.Data;

@Data
public class User {

    private String userId;

    private String userName;

    private String email;


}
