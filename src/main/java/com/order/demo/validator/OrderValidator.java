package com.order.demo.validator;

import com.order.demo.configuration.OrderPropertyConfiguration;
import com.order.demo.model.Item;
import com.order.demo.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class OrderValidator {

    private final OrderPropertyConfiguration orderPropertyConfiguration;

    public OrderValidator(OrderPropertyConfiguration orderPropertyConfiguration) {
        this.orderPropertyConfiguration = orderPropertyConfiguration;
    }

    public boolean checkItemsAvailability(List<Item> items){

        log.info("Start checkOrderAvailability");
        for(Item item : items){
            if(Boolean.FALSE.equals(item.isAvailable())){
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public boolean validateOrderAmount(Order order){

        log.info("Start validateOrderAmount with userId {}", order.getUserId());
        if(order.getTotalAmount() < Double.valueOf(orderPropertyConfiguration.getValidation().getMinOrderAmount())){
            log.error("Invalid order amount with userId {}", order.getUserId());
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public boolean isFraudDetected(Order order){

        log.info("Start isFraudDetected with userId {}", order.getUserId());
        if(order.getTotalAmount() > Double.valueOf(orderPropertyConfiguration.getValidation().getMinOrderBasketAmount())){
            log.error("Order fraud detected with userId {}", order.getUserId());
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
