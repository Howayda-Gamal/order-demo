create table user
(
   user_id int not null AUTO_INCREMENT,
   user_name varchar(255) not null,
   email varchar(255) not null,
   primary key (user_id)
);

create table card
(
    card_id int not null AUTO_INCREMENT,
    card_type varchar(255),
    card_number varchar(255),
    ccv varchar(255),
    expiry_date varchar(255),
    user_Id int,
    primary key (card_id),
    CONSTRAINT FK_User_Card FOREIGN KEY (user_id)
        REFERENCES user(user_id)
);
