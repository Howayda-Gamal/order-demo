1- Once the project is ready to run , just right click on project and choose 'run as' then 'Spring Boot App'

2- I am using h2 as an in-memory database and i created two files 'schema.sql' and 'data.sql' to create the table and insert some data into it, it will run automatically during spring boot app initialization.

3- After starting 'on port 8080' , go to 'http://localhost:8080/h2-console' and press connect to activate h2 database. make sure that the JDBC field has this value : 'jdbc:h2:mem:testdb' ,After connecting , you can see tables 'Card and User' and some records in it.

4- To perform the app ,, you can use first unit testing for Service that exist in test package.

5- For testing the rest api , i am using tool for http request 'postman'. it is a POST method and 
the url in postman is localhost:8080/api/checkout
example for JSON Request : {

    {
        "items":[
        {
            "itemId": 1,
            "itemName": "a",
            "price": 100.0,
            "available": true
        },
         {
            "itemId": 2,
            "itemName": "b",
            "price": 11.0,
            "available": true
        }
        ],
        "userId": 1,
        "totalAmount": 1500.0
    }
}
successful response for this request is : {

    {
    "statusCode": "0",
    "message": "Success"
    }
}
and the failure response is : {

    {
    "statusCode": "1",
    "message": "Failed to checkout"
    }
}

That's all for now , if you faced any unexpected error , contact me on howayda.gamal@gmail.com